import sys

size = sys.argv[1]
filename = sys.argv[2]
# output_folder = 'output/' + size + '/'
output_folder = ''

def output_to_cnf(filename):
    with open(output_folder + filename) as f:
        content = f.readlines()

    atoms = []

    for index, line in enumerate(content):
        if index != 0:
            atoms = map(int, line.split(' ')[:-1])

    return [[atom] for atom in atoms]

def encode_dimacs(cnf):
    return ("p cnf %d %d\n" % (int(size) ** 3, len(cnf))) + "\n".join([" ".join(map(str, rule)) + " 0" for rule in cnf])

cnf_file = open('output_goed.cnf', 'w+')
cnf_file.write(encode_dimacs(output_to_cnf(filename)))
