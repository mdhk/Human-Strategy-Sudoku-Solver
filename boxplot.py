import numpy as np
import matplotlib.pyplot as plt

with open('final_stats/16/human/final_conflicts.txt') as f:
    content_1 = f.readline()
with open('final_stats/16/minimal/final_conflicts.txt') as f:
    content_2 = f.readline()
with open('final_stats/16/efficient_human/final_conflicts.txt') as f:
    content_3 = f.readline()

data_1 = content_1.split(',')
data_2 = content_2.split(',')
data_3 = content_3.split(',')

int_data_1 = [int(x) for x in data_1[:-1]]
int_data_2 = [int(x) for x in data_2[:-1]]
int_data_3 = [int(x) for x in data_3[:-1]]

fig, axes = plt.subplots(nrows=3, ncols=1, figsize=(6, 6), sharex=True)

axes[0].boxplot(int_data_1, vert=False, showfliers=True, autorange=False)
axes[1].boxplot(int_data_2, vert=False, showfliers=True, autorange=False)
axes[2].boxplot(int_data_3, vert=False, showfliers=True, autorange=False)

for axis in axes.flatten():
    axis.set_xscale('log')
    axis.set_xticklabels([])

fig.subplots_adjust(hspace=0.4)
plt.show()
