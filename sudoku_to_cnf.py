import numpy as np
import sys
from itertools import product
from os import listdir
from os.path import isfile, join
from tqdm import *

start_index = sys.argv[1]
end_index = sys.argv[2]

# Sudoku Constants
size = 16
size_sq = int(size ** 0.5)

enable_human_technique = True
recalculate = True

sudoku_folder = 'sudokus/' + str(size) + '/'
sudokus = [f for f in listdir(sudoku_folder) if isfile(join(sudoku_folder, f))]

print('Converting Sudokus to CNF with the following parameters:')
print('Size: ' + str(size) + 'x' + str(size))
print('Enable human techniques: ' + str(enable_human_technique))


def glue(x, y, z):
    return '%d_%d_%d' % (x, y, z)


mapping_array = [glue(x, y, z) for x, y in product(range(size), repeat=2) for z in np.arange(size) + 1]


def q(str):
    return mapping_array.index(str) + 1


def parse_sudoku(filename):
    with open(filename) as f:
        content = f.readline()

    return np.array_split(content.split(','), size)


# BASIC ENCODING
def calculate_basic_cnf(cnf):
    for x, y in product(range(size), repeat=2):
        cnf.extend([[q(glue(x, y, z)) for z in np.arange(size) + 1]])

    for z in np.arange(size) + 1:
        for y in range(size):
            for x in range(size - 1):
                for i in range(x + 1, size):
                    cnf.extend([[-1 * q(glue(x, y, z)), -1 * q(glue(i, y, z))]])
        for x in range(size):
            for y in range(size - 1):
                for i in range(y + 1, size):
                    cnf.extend([[-1 * q(glue(x, y, z)), -1 * q(glue(x, i, z))]])
        for i, j in product(range(size_sq), repeat=2):
            for x, y in product(range(size_sq), repeat=2):
                for k in range(y + 1, size_sq):
                    cnf.extend([[-1 * q(glue(size_sq * i + x, size_sq * j + y, z)),
                                 -1 * q(glue(size_sq * i + x, size_sq * j + k, z))]])
                for k in range(x + 1, size_sq):
                    for l in range(size_sq):
                        cnf.extend([[-1 * q(glue(size_sq * i + x, size_sq * j + y, z)),
                                     -1 * q(glue(size_sq * i + k, size_sq * j + l, z))]])

    return cnf


# HUMAN STRATEGY

def calculate_human_strategy_cnf(sudoku):
    # The human strategy form is an addition to the minimal encoding
    cnf, redundant_variables = row_elimination([], sudoku, [])
    cnf, redundant_variables = column_elimination(cnf, sudoku, redundant_variables)
    cnf, redundant_variables = block_elimination(cnf, sudoku, redundant_variables)

    # We won't need the redundant literals here, since there is always a literal true in this clause
    for x, y in product(range(size), repeat=2):
        cnf.extend([[q(glue(x, y, z)) for z in np.arange(size) + 1]])

    for z in np.arange(size) + 1:
        for y in range(size):
            for x in range(size - 1):
                for i in range(x + 1, size):
                    literal_1 = q(glue(x, y, z))
                    literal_2 = q(glue(i, y, z))
                    if literal_1 not in redundant_variables and literal_2 not in redundant_variables:
                        cnf.extend([[-1 * literal_1, -1 * literal_2]])
        for x in range(size):
            for y in range(size - 1):
                for i in range(y + 1, size):
                    literal_1 = q(glue(x, y, z))
                    literal_2 = q(glue(x, i, z))
                    if literal_1 not in redundant_variables and literal_2 not in redundant_variables:
                        cnf.extend([[-1 * literal_1, -1 * literal_2]])
        for i, j in product(range(size_sq), repeat=2):
            for x, y in product(range(size_sq), repeat=2):
                for k in range(y + 1, size_sq):
                    literal_1 = q(glue(size_sq * i + x, size_sq * j + y, z))
                    literal_2 = q(glue(size_sq * i + x, size_sq * j + k, z))
                    if literal_1 not in redundant_variables and literal_2 not in redundant_variables:
                        cnf.extend([[-1 * literal_1, -1 * literal_2]])
                for k in range(x + 1, size_sq):
                    for l in range(size_sq):
                        literal_1 = q(glue(size_sq * i + x, size_sq * j + y, z))
                        literal_2 = q(glue(size_sq * i + k, size_sq * j + l, z))
                        if literal_1 not in redundant_variables and literal_2 not in redundant_variables:
                            cnf.extend([[-1 * literal_1, -1 * literal_2]])

    return cnf


# Row elimination checks whether a given number has already appeared in a certain row. If so, all
# the other cells in the same row cannot be this number.
def row_elimination(cnf, sudoku, redundant_literals):
    # Loop over all rows
    for y, row in enumerate(sudoku):
        for x, cell in enumerate(row):
            if cell != '0':
                for i in np.arange(size):
                    if i != x and row[i] == '0':
                        literal = q(glue(i, y, int(cell)))
                        cnf.extend([[-1 * literal]])
                        redundant_literals.append(literal)

    return cnf, redundant_literals


# Column elimination checks whether a given number has already appeared in a certain column. If so, all the other cells
# in the same column cannot be this number.
def column_elimination(cnf, sudoku, redundant_literals):
    # First, transpose the sudoku, so we can easily loop over the columns
    sudoku = np.transpose(sudoku)
    # Loop over all columns
    for x, column in enumerate(sudoku):
        for y, cell in enumerate(column):
            if cell != '0':
                for i in np.arange(size):
                    if i != y and column[i] == '0':
                        literal = q(glue(x, i, int(cell)))
                        cnf.extend([[-1 * literal]])
                        redundant_literals.append(literal)

    return cnf, redundant_literals


# Block elimination checks wheether a given number has already appeared in a certain block. If so, all the other cells
# in the same block cannot be this number.
def block_elimination(cnf, sudoku, redundant_literals):
    # Select all blocks
    for x_off, y_off in product(np.arange(size_sq), repeat=2):
        current_block = np.asarray(sudoku)[(x_off * size_sq):((x_off + 1) * size_sq),
                        (y_off * size_sq):((y_off + 1) * size_sq)]
        for b_y, b_r in enumerate(current_block):
            for b_x, cell in enumerate(b_r):
                if cell != '0':
                    literals = [[-1 * q(glue(y_off * size_sq + y_off_temp, x_off * size_sq + x_off_temp, int(cell)))]
                                if b_y != x_off_temp or b_x != y_off_temp
                                else None
                                for x_off_temp, y_off_temp in product(np.arange(size_sq), repeat=2)]
                    for literal in literals:
                        if literal != None:
                            redundant_literals.append(literal[0])
                    cnf.extend(literals)

    return list(filter(lambda a: a != None, cnf)), redundant_literals


def sudoku_as_cnf(sudoku):
    cnf = []

    # When recalculate is false, use the cached version of the model
    if (recalculate == False):
        content = []

        with open('cnf/template_' + str(size) + '.cnft') as f:
            content = f.readlines()
            cnf = [[int(atom) for atom in line.split(" ")[:-1]] for line in content]

        if enable_human_technique:
            cnf, redundant_variables = row_elimination(cnf, sudoku, [])
            cnf, redundant_variables = column_elimination(cnf, sudoku, redundant_variables)
            cnf, redundant_variables = block_elimination(cnf, sudoku, redundant_variables)
            for clause in cnf:
                if len(clause) == 2 and clause[0] in redundant_variables and clause[1] in redundant_variables:
                    cnf.remove(clause)

    # Otherwise, calculate it again
    else:
        cnf = []
        if enable_human_technique:
            cnf = calculate_human_strategy_cnf(sudoku)
        else:
            cnf = calculate_basic_cnf(cnf)

    for y, row in enumerate(sudoku):
        for x, cell in enumerate(row):
            if cell != '0':
                cnf.extend([[q(glue(x, y, int(cell)))]])

    return cnf


def encode_dimacs(cnf):
    return ("p cnf %d %d\n" % (size ** 3, len(cnf))) + "\n".join([" ".join(map(str, rule)) + " 0" for rule in cnf])


def convert_all_sudokus():
    for index, sudoku in enumerate(tqdm(sudokus)):
        if not index < int(start_index) and not index > int(end_index):
            data = parse_sudoku(sudoku_folder + sudoku)
            filename = sudoku.split('.')[0]

            cnf_file = open('cnf/' + str(size) + '/sudoku_' + filename + '.cnf', 'w+')
            cnf_file.write(encode_dimacs(sudoku_as_cnf(data)))


convert_all_sudokus()
# cnf_file = open('onzin_human.cnf', 'w+')
# data = parse_sudoku('sudokus/16/4050.sudoku')
# cnf = calculate_basic_cnf([])
# cnf = calculate_human_strategy_cnf(data)
# for y, row in enumerate(data):
#     for x, cell in enumerate(row):
#         if cell != '0':
#             cnf.extend([[q(glue(x, y, int(cell)))]])
#
# cnf_file.write(encode_dimacs(cnf))

# cnf_file.write(encode_dimacs(calculate_human_strategy_cnf(data)))
# cnf_file.write(encode_dimacs(sudoku_as_cnf(data)))