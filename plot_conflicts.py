import matplotlib.pyplot as plt
import sys
import numpy as np
import seaborn as sns
sns.set(color_codes=True)

# run with command:
# python plot_conflicts.py final_stats/16/minimal/final_conflicts.txt final_stats/16/human/final_conflicts.txt final_stats/16/efficient_human/final_conflicts.txt

colors = ['b', 'g', 'r']

bins = [x for x in np.arange(0, 2000, 100)]

for i in range(1, len(sys.argv)):
    with open(sys.argv[i]) as f:
        content = f.readline()

    data = content.split(',')
    int_data = [int(x) for x in data[:-1]]

    plot = sns.distplot(int_data, bins=bins, kde=False, \
                        axlabel='Number of conflicts',\
                        label=(sys.argv[i].replace('final_stats/16/', '').replace('/final_conflicts.txt', '') + ' encoding'))
    plot.set(xlim=(0, 2000), ylim=(0,400))
    plot.axvline(np.asarray(int_data).mean(), color=colors[i-1], linestyle='dashed', linewidth=2)
    plot.set_ylabel('Number of sudokus')

plot.legend()
plot.figure.savefig("final_stats/16/plots/conflicts_histogram.png")

## code for regular matplotlib histograms:
# with open(sys.argv[1]) as f:
#     content = f.readline()
#
# data = content.split(',')
# int_data = [int(x) for x in data[:-1]]
#
# bins = [x for x in np.arange(0, 2000, 100)]
#
# plt.hist(int_data, bins=bins, color="c")
#
# # y = mlab.normpdf( bins, mu, sigma)
# # l = plt.plot(bins, y, 'r--', linewidth=1)
# plt.axvline(np.asarray(int_data).mean(), color='b', linestyle='dashed', linewidth=2)
# plt.axis([0, 2000, 0, 150])
# plt.grid(False)
#
# plt.title('Conflicts 16x16 sudoku (minimal encoding)')
# plt.xlabel('Amount of conflicts')
# plt.ylabel('Amount of sudokus')
#
# plt.show()