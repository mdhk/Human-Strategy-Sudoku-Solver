# Human Strategy Sudoku Solver
## Description
This Sudoku Solver uses various different human sudoku solving techniques in order to minimize the amounts of conflicts, restarts etc.
## Requirements
- numpy
- itertools
- tqdm
- matplotlib
- MiniSAT (already compiled, you can find the used version here: https://github.com/pascalesser/minisat)
## How to use
- Run `python sudoku_to_cnf.py` to convert the sudokus from the `sudokus/size/` folder to a suitable `.cnf` format
- Run `python sudoku_solver.py <size>` to automatically solve all sudokus of the given size, the statistics will be written to the `stat/` folder
- Run `python sudoku_visualizer.py <size> <cnf>` to visualize a certain sudoku solution from MiniSAT
