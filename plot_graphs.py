import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
sns.set(color_codes=True)

colors = ['b', 'g', 'r']

fig, axes = plt.subplots(3, 4, figsize=(15,10), sharey=False)

# # conflict histograms
# bins = [x for x in np.arange(0, 2000, 100)]
# conflicts_stats = ['final_stats/16/minimal/final_conflicts.txt', 'final_stats/16/human/final_conflicts.txt', \
#                    'final_stats/16/efficient_human/final_conflicts.txt']
#
# for i in range(0, len(conflicts_stats)):
#     with open(conflicts_stats[i]) as f:
#         content = f.readline()
#
#     data = content.split(',')
#     int_data = [int(x) for x in data[:-1]]
#
#     plot = sns.distplot(int_data, bins=bins, kde=False, \
#                         axlabel='Number of conflicts',\
#                         label=(conflicts_stats[i].replace('final_stats/16/', '').replace('/final_conflicts.txt', '') + ' encoding'))
#     plot.set(xlim=(0, 2000), ylim=(0,400))
#     plot.axvline(np.asarray(int_data).mean(), color=colors[i], linestyle='dashed', linewidth=2)
#     plot.set_ylabel('Number of sudokus')
#
# plot.legend()
# plot.figure.savefig("final_stats/16/plots/conflicts_histogram.png")
#
# # clear figure
# plot.clear()

# CPU time boxplots
CPUtime_stats = {'minimal': 'final_stats/16/minimal/final_cpu_time.txt', 'human': 'final_stats/16/human/final_cpu_time.txt', \
                 'efficient_human': 'final_stats/16/efficient_human/final_cpu_time.txt'}

cpu_times = []
encodings = []

for i in range(len(CPUtime_stats.keys())):
    with open(CPUtime_stats[list(CPUtime_stats.keys())[i]]) as f:
        cpu_times_i = [float(x) for x in f.readline().split(',') if x]
        cpu_times.extend(cpu_times_i)
        encodings.extend([list(CPUtime_stats.keys())[i]] * len(cpu_times_i))

df = pd.DataFrame({'Encodings': encodings, 'CPU times': cpu_times})

sns.boxplot(x='Encodings', y='CPU times', data=df, orient='v', ax=axes[0,0], showfliers=False)
axes[0,0].set(ylim=(0.0,1.5))

# CPU time histograms
for i in range(0, len(list(CPUtime_stats.values()))):
    with open(list(CPUtime_stats.values())[i]) as f:
        content = f.readline()

    data = content.split(',')
    float_data = [float(x) for x in data[:-1]]

    plot = sns.distplot(float_data, bins=50, kde=False, axlabel='CPU time', ax=axes[0, i+1], color=colors[i], \
                        label=list(CPUtime_stats.keys())[i])
    plot.legend()
    plot.set_ylabel('Number of sudokus')
    plot.set_ylim(0,300)
    plot.set_xlim(0,2.0)
    plot.axvline(np.asarray(float_data).mean(), color=colors[i], linestyle='dashed', linewidth=2)


# conflicts boxplots
conflicts_stats = {'minimal': 'final_stats/16/minimal/final_conflicts.txt', 'human': 'final_stats/16/human/final_conflicts.txt', \
                 'efficient_human': 'final_stats/16/efficient_human/final_conflicts.txt'}

conflicts = []
encodings = []

for i in range(len(conflicts_stats.keys())):
    with open(conflicts_stats[list(conflicts_stats.keys())[i]]) as f:
        conflicts_i = [int(x) for x in f.readline().split(',') if x]
        conflicts.extend(conflicts_i)
        encodings.extend([list(CPUtime_stats.keys())[i]] * len(conflicts_i))

df2 = pd.DataFrame({'Encodings': encodings, 'Conflicts': conflicts})

sns.boxplot(x='Encodings', y='Conflicts', data=df2, orient='v', ax=axes[1, 0], showfliers=False)
axes[1, 0].set(ylim=(0,2500))

# conflicts histograms
bins = [x for x in np.arange(0, 2000, 50)]
for i in range(0, len(list(conflicts_stats.values()))):
    with open(list(conflicts_stats.values())[i]) as f:
        content = f.readline()

    data = content.split(',')
    int_data = [int(x) for x in data[:-1]]

    plot = sns.distplot(int_data, bins=bins, kde=False, axlabel='Number of conflicts', ax=axes[1, i+1], color=colors[i], \
                        label=list(conflicts_stats.keys())[i])
    plot.legend()
    plot.set(xlim=(0, 2000), ylim=(0,400), ylabel='Number of sudokus')
    plot.axvline(np.asarray(int_data).mean(), color=colors[i], linestyle='dashed', linewidth=2)

# restarts boxplots
restarts_stats = {'minimal': 'final_stats/16/minimal/final_restarts.txt', 'human': 'final_stats/16/human/final_restarts.txt', \
                 'efficient_human': 'final_stats/16/efficient_human/final_restarts.txt'}

restarts = []
encodings = []

for i in range(len(restarts_stats.keys())):
    with open(restarts_stats[list(restarts_stats.keys())[i]]) as f:
        restarts_i = [int(x) for x in f.readline().split(',') if x]
        restarts.extend(restarts_i)
        encodings.extend([list(CPUtime_stats.keys())[i]] * len(restarts_i))

df3 = pd.DataFrame({'Encodings': encodings, 'Restarts': restarts})

sns.boxplot(x='Encodings', y='Restarts', data=df3, orient='v', ax=axes[2, 0], showfliers=False)
axes[2, 0].set(ylim=(0,15))

# restarts histograms
for i in range(0, len(list(restarts_stats.values()))):
    with open(list(restarts_stats.values())[i]) as f:
        content = f.readline()

    data = content.split(',')
    int_data = [int(x) for x in data[:-1]]

    plot = sns.distplot(int_data, bins=50, kde=False, axlabel='Number of restarts', ax=axes[2, i+1], color=colors[i], \
                        label=list(restarts_stats.keys())[i])
    plot.legend()
    plot.set(xlim=(0, 30), ylim=(0,300), ylabel='Number of sudokus')
    plot.axvline(np.asarray(int_data).mean(), color=colors[i], linestyle='dashed', linewidth=2)

plt.tight_layout()

for row_i, row in enumerate(axes):
    for col_i, col in enumerate(row):
        extent = axes[row_i, col_i].get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        plt.savefig('final_stats/16/plots/ax' + str(row_i) + str(col_i), bbox_inches=extent.expanded(1.3, 1.3).translated(-0.23, -0.2))

# save entire figure
plt.savefig('final_stats/16/plots/graphs.png')
