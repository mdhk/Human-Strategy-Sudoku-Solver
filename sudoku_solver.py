from subprocess import call
import sys
from os import listdir
from os.path import isfile, join

size = int(sys.argv[1])

cnf_folder = '/home/christian/Documents/Sudoku-Solver/cnf/' + str(size) + '/'
cnfs = [f for f in listdir(cnf_folder) if isfile(join(cnf_folder, f))]

minisat_path = 'build'
minisat_executable = 'minisat'

output = '/home/christian/Documents/Sudoku-Solver/output/' + str(size) + '/'

for cnf in cnfs:
    call(['./' + minisat_executable, cnf_folder + cnf, output + cnf])
