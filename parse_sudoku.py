import re
import numpy as np

with open('data/sudoku.csv') as f:
    content = f.readlines()

content = [x.strip() for x in content]

for y, line in enumerate(content):
    if y == 0:
        continue
    if y == 1000:
        break

    split = line.split(',')
    exercise_file = open('sudokus/' + str(np.math.floor(y / 100)) + '/sudoku_' + str(y) + '.exercise', 'w+')
    solution_file = open('sudokus/' + str(np.math.floor(y / 100)) + '/sudoku_' + str(y) + '.solution', 'w+')
    print(y)
    exercise_file.write(split[0])
    solution_file.write(split[1])