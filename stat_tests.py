import numpy as np
from scipy import stats

CPUtime_stats = {'minimal': 'final_stats/16/minimal/final_cpu_time.txt', 'human': 'final_stats/16/human/final_cpu_time.txt', \
                 'efficient_human': 'final_stats/16/efficient_human/final_cpu_time.txt'}

cpu_times = {}

for i in range(len(CPUtime_stats.keys())):
    with open(CPUtime_stats[list(CPUtime_stats.keys())[i]]) as f:
        cpu_times_i = [float(x) for x in f.readline().split(',') if x]
        cpu_times[list(CPUtime_stats.keys())[i]] = cpu_times_i

conflicts_stats = {'minimal': 'final_stats/16/minimal/final_conflicts.txt', 'human': 'final_stats/16/human/final_conflicts.txt', \
                 'efficient_human': 'final_stats/16/efficient_human/final_conflicts.txt'}

conflicts = {}

for i in range(len(conflicts_stats.keys())):
    with open(conflicts_stats[list(conflicts_stats.keys())[i]]) as f:
        conflicts_i = [int(x) for x in f.readline().split(',') if x]
        conflicts[list(conflicts_stats.keys())[i]] = conflicts_i

restarts_stats = {'minimal': 'final_stats/16/minimal/final_restarts.txt', 'human': 'final_stats/16/human/final_restarts.txt', \
                 'efficient_human': 'final_stats/16/efficient_human/final_restarts.txt'}

restarts = {}

for i in range(len(restarts_stats.keys())):
    with open(restarts_stats[list(restarts_stats.keys())[i]]) as f:
        restarts_i = [int(x) for x in f.readline().split(',') if x]
        restarts[list(restarts_stats.keys())[i]] = restarts_i

print('CPU time:\n' +
      'minimal vs. efficient_human', str(stats.ks_2samp(cpu_times['minimal'], cpu_times['efficient_human'])) + '\n' +
      'minimal vs. human:', str(stats.ks_2samp(cpu_times['minimal'], cpu_times['human'])) + '\n' +
      'human vs. efficient_human:', str(stats.ks_2samp(cpu_times['human'], cpu_times['efficient_human'])) + '\n')

print ('Mean CPU times:\n' +
       'minimal:', str(np.mean(cpu_times['minimal'])) + '\n' +
       'human:', str(np.mean(cpu_times['human'])) + '\n' +
       'efficient_human:', str(np.mean(cpu_times['efficient_human'])) + '\n')

print('Conflicts:\n' +
      'minimal vs. efficient_human:', str(stats.ks_2samp(conflicts['minimal'], conflicts['efficient_human'])) + '\n' +
      'minimal vs. human:', str(stats.ks_2samp(conflicts['minimal'], conflicts['human'])) + '\n' +
      'human vs. efficient_human:', str(stats.ks_2samp(conflicts['human'], conflicts['efficient_human'])) + '\n')

print('Restarts:\n' +
      'minimal vs. efficient_human:', str(stats.ks_2samp(restarts['minimal'], restarts['efficient_human'])) + '\n' +
      'minimal vs. human:', str(stats.ks_2samp(restarts['minimal'], restarts['human'])) + '\n' +
      'human vs. efficient_human:', str(stats.ks_2samp(restarts['human'], restarts['efficient_human'])))
