import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import sys

with open(sys.argv[1]) as f:
    content = f.readline()

data = content.split(',')
int_data = [int(x) for x in data[:-1]]

bins = [x for x in np.arange(0, 50, 5)]

plt.hist(int_data, bins=bins, color="c")

# y = mlab.normpdf( bins, mu, sigma)
# l = plt.plot(bins, y, 'r--', linewidth=1)
plt.axvline(np.asarray(int_data).mean(), color='b', linestyle='dashed', linewidth=2)
plt.axis([0, 50, 0, 750])
plt.grid(False)

plt.title('Restarts 16x16 sudoku (minimal encoding)')
plt.xlabel('Amount of restarts')
plt.ylabel('Amount of sudokus')

plt.show()
