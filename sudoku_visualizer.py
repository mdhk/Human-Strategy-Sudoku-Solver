import numpy as np
import sys
from itertools import product

def glue(x, y, z):
    return x, y, z

output = sys.argv[2]
size = int(sys.argv[1])

sudoku = [[0 for i in range(size)] for j in range(size)]

mapping_array = [glue(x,y,z) for x, y in product(range(size), repeat=2) for z in np.arange(size) + 1]

def get_value(index):
    row, col, val = mapping_array[int(index) - 1]
    if val == 0:
        val = size
        col -= 1

    return row, col, val

with open(output) as f:
    content = f.readlines()
    values = filter(lambda x: int(x) > 0, (content[1].split(' ')))
    result = list(map(get_value, values))
    for r in result:
        split = r
        sudoku[int(split[0])][int(split[1])] = int(split[2])

for row in sudoku:
    print(row)
